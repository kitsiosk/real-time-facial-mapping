Python program to detect 68 face landmarks in real time from webcam frames.

The pretrained dlib model can be downloaded here: http://dlib.net/files/shape_predictor_68_face_landmarks.dat.bz2