"""
This program uses your webcam to detect your face and
plot 68 landmarks representing the core characteristics of it, namely
mouth, nose, left eye, right eye, left eyebrow, right eyebrow, jaw.
For this purpose, a pretrained Dlib model is used.
"""

import cv2
import dlib
from imutils import face_utils

# Directory of the pretrained model
p = "../shape_predictor_68_face_landmarks.dat"

detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(p)

# if (you have only 1 webcam){ set device = 0} else{ chose your favorite webcam setting device = 1, 2 ,3 ... }
cap = cv2.VideoCapture(0)

while True:
  # Getting our image by webcam and converting it into a gray image scale
    _, image = cap.read()
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # Get faces into webcam's image
    rects = detector(gray, 0)

    for rect in rects:
        # Make the prediction and transfom it to numpy array
        shape = predictor(gray, rect)
        shape = face_utils.shape_to_np(shape)

        # Draw on our image, all the finded cordinate points (x,y) 
        for (x, y) in shape:
            cv2.circle(image, (x, y), 2, (0, 255, 0), -1)

    # show the gray image
    cv2.imshow("Output", image)
    
    #key to give up the app.
    k = cv2.waitKey(5) & 0xFF
    if k == ord('q'):
        break
cv2.destroyAllWindows()
cap.release()